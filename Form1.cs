﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Threading;
using System.Timers;

namespace FileSearch
{
    public partial class FileFinder : Form
    {
        public FileFinder()
        {
            InitializeComponent();                    
        }
    
        private FolderBrowserDialog ShowFolder = new FolderBrowserDialog();
        private TaskCompletionSource<bool> asyncBusy;           
        private int min, sec, hr;
        private CancellationTokenSource cts;
        private CancellationToken token;
        private System.Timers.Timer timer;       

        private void textBox1_Click(object sender, EventArgs e)
        {           
            if (ShowFolder.ShowDialog() == DialogResult.OK)
            {
                textBox1.Text = ShowFolder.SelectedPath;              
            }
        }      
        
        private void SaveCriteria()
        {
            using (BinaryWriter info = new BinaryWriter(File.Create("criteria.bin")))
            {
                info.Write(ShowFolder.SelectedPath);
                info.Write(textBox2.Text);
                info.Write(textBox3.Text);
            }
        } 

        private void LoadCriteria()
        {
            try
            {
                using (BinaryReader info = new BinaryReader(File.OpenRead("criteria.bin")))
                {
                    string showFolderDirectory = info.ReadString();
                    string fileNamePattern = info.ReadString();
                    string textToSearch = info.ReadString();

                    ShowFolder.SelectedPath = showFolderDirectory;
                    textBox1.Text = ShowFolder.SelectedPath;
                    textBox2.Text = fileNamePattern;
                    textBox3.Text = textToSearch;                  
                }
            }
            catch (Exception)
            {

            }
        }    

        private void CurrentProgress(int index, int total)
        {
            progressBar.Value = (index * 100 / total);          
            currentprogress.Text = string.Format($"Выполнен на {progressBar.Value}%");                                      
        }      
      
        private void CountFilesProgress(int i)
        {
            countfileslbl.Text = i.ToString();
            currentfile.Text = "Файл: " + listBox1.Items[i - 1];
        }

        private async void DoWorkAsync() 
        {
            string textToSearch = textBox3.Text;
            string filePatternName = textBox2.Text;
            string Dir = ShowFolder.SelectedPath;
            int index = 1;

            cts = new CancellationTokenSource();
            token = cts.Token;
            int totalNumbersOfPatternNameWithOutOftextToSearch = SearchHelpers.CountFiles(Dir, filePatternName);
            SearchHelpers.ListContainer(Dir, filePatternName, listBox1);       
           
            for (int i = 1; i <= totalNumbersOfPatternNameWithOutOftextToSearch; i++)
            {
                if (token.IsCancellationRequested)
                {
                   return;
                }

                if (startBtn.Text == "Пуск")
                {
                    asyncBusy = new TaskCompletionSource<bool>();
                    await asyncBusy.Task;
                }
                
                CurrentProgress(index++, totalNumbersOfPatternNameWithOutOftextToSearch);
                CountFilesProgress(i);
                await Task.Delay(50);              
            } 
            
            SearchHelpers.LoadDirectory(Dir, treeView1, textToSearch, 
                filePatternName, startBtn, timer, listBox1);
            
        }

        private void stopBtn_Click(object sender, EventArgs e)
        {                                
             cts.Cancel();

             startBtn.Text = "Начать поиск";
             timer.Stop();
             treeView1.Nodes.Clear();
             listBox1.Items.Clear();

             MessageBox.Show("Поиск прерван", "Отменено", 
                 MessageBoxButtons.OK, MessageBoxIcon.Information);
             progressBar.Value = 0;
             currentprogress.Text = "Поиск файлов";
             currentfile.Text = "Файл:";
             countfileslbl.Text = "";
             lbltime.Text = "00:00:00";                                  
        }

        private  void startBtn_Click(object sender, EventArgs e)
        {            
            treeView1.Nodes.Clear();
           
            string fileNamePattern = textBox2.Text;
            string textInFile = textBox3.Text;
            string Dir = ShowFolder.SelectedPath;

            if (Dir == "" || fileNamePattern == "" || textInFile == "")
            {               
                MessageBox.Show("Не все поля заполнены!", "Ошибка", 
                    MessageBoxButtons.OK, MessageBoxIcon.Error);               
            }
      
            else if (startBtn.Text == "Остановить")
            {                              
                timer.Stop();
                startBtn.Text = "Пуск";
            }
            else if (startBtn.Text == "Пуск")
            {
                asyncBusy.SetResult(false);
                timer.Start();
                startBtn.Text = "Остановить";                               
            }
            
            else 
            {
                startBtn.Text = "Остановить";
                DoWorkAsync();
                timer.Start();
                lbltime.Text = "00:00:00";
            }
        }

        private void TextBox_Leave(object sender, EventArgs e)
        {
            SaveCriteria();
        }
                             
        private void Form1_Load(object sender, EventArgs e)
        {
            LoadCriteria();

            progressBar.Value = 0;
            countfileslbl.Text = "";
            lbltime.Text = "";

            timer = new System.Timers.Timer();
            timer.Interval = 1000;
            timer.Elapsed += TimeEvent;
        }
        
        private void TimeEvent(object sender, System.Timers.ElapsedEventArgs e)
        {
            Invoke(new Action(() =>
            {
                if (lbltime.Text == "00:00:00")
                {
                    sec = 0;
                    min = 0;
                    hr = 0;
                }
                sec += 1;
                if (sec == 60)
                {
                    sec = 0;
                    min += 1;
                }
                if (min == 60)
                {
                    min = 0;
                    hr += 1;
                }
                string formatting = string.Format($"{hr.ToString().PadLeft(2, '0')}:{min.ToString().PadLeft(2, '0')}" +
                    $":{sec.ToString().PadLeft(2, '0')}");
                lbltime.Text = formatting;               
            }));
            
        }                
    }
}
